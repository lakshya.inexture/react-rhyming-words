var server_echo;
var json = {
    json: JSON.stringify({
        a: 1,
        b: 2,
    }),
    delay: 3,
};
fetch("/echo/", {
    method: "post",
    headers: {
        Accept: "application/json, text/plain, */*",
        "Content-Type": "application/json",
    },
    body:
        "json=" +
        encodeURIComponent(JSON.stringify(json.json)) +
        // if you stringify json.json again, it will convert a json string into another json string. Only need to write it once
        "&delay=" +
        json.delay,
})
    .then(function (response) {
        server_echo = response.json().echo;
        // cannot use .echo here because in the first .then, you only get a promise in return. So need to remove the return statement as well because we are binding the promise to the server_echo. To get .echo, need to write another .then.
        return response.json();
    })
    .then(function (result) {
        alert(result);
    })
    .catch(function (error) {
        console.log("Request failed", error);
    });
server_echo.forEach((element) => console.log(element));
