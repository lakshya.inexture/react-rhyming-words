import React, { useState } from "react";

function App() {
    const [word, setWord] = useState("");
    const [rhymes, setRhymes] = useState([]);
    const url = `https://api.datamuse.com/words?rel_rhy=${word}`;

    const getRhymes = () => {
        fetch(url)
            .then((res) => {
                return res.json();
            })
            .then((data) => {
                return setRhymes(data);
            });
    };

    const clearRhymes = () => {
        setRhymes([]);
        setWord("");
    };
    return (
        <div style={{ fontFamily: "monospace", fontSize: "24px" }}>
            <h1>Find Rhyming Words</h1>
            <input
                type="text"
                value={word}
                onChange={(e) => setWord(e.target.value)}
            />
            <button onClick={getRhymes}>Get rhyming words</button>
            <button onClick={clearRhymes}>Clear</button>
            <ul>
                {rhymes.map((e, i) => {
                    return <li key={i}>{e.word}</li>;
                })}
            </ul>
        </div>
    );
}

export default App;
